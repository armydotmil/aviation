var keyword = 'target_aviation';

var Article = Backbone.Model.extend({
	urlRoot: 'http://www.army.mil/api/packages/getpackagesbykeywords?keywords=' + keyword + '&offset=1&count=100'
});

var Articles = Backbone.Collection.extend({
	model: Article,
	url: 'http://www.army.mil/api/packages/getpackagesbykeywords?keywords=' + keyword + '&offset=1&count=100'
});

var articles = new Articles();

var ArticleGallery = Backbone.View.extend({
	initialize: function() {
		this.listenTo(articles, 'sync', this.load_articles);
		articles.fetch();
	},
	load_articles: function() {
		var articles_div = $('div.articles');
		
		var numThumbNails = 3;
		
		var articles_more = $('div.articles_more');
		
		if(articles_more.length > 0){
			numThumbNails = 5;
		}
		
		for (var i = 0; i < numThumbNails; i++) {
			articles_div.append(build_article_cell(articles.shift()));
		}
		
		$('div.article:last').css('border-bottom-width',0);

		var more_stories_list = $("ul.more_stories_list");
		if (more_stories_list) {
			for (var j = 0; j < articles.length; j++) {
				more_stories_list.append(build_article_row(articles.shift()));
			}
		}
	}
});

function build_article_row(article) {
	var title = article.get('title');

	var max_str_length = 40;
	if (title.length > max_str_length) {
		title = title.substring(0, max_str_length);
		title += "...";
	}

	var page_url = article.get('page_url');
	var date = article.get('date');
	var li = $('<li>');
	var a = $('<a>').prop({
		'href': page_url
	});

	var span = $('<span>').html(stringToDate(date).toDateString());

	a.html(title)

	return li.append(a).append(span);
}

function stringToDate(s) {
	s = s.split(/[-: ]/);
	return new Date(s[0], s[1] - 1, s[2], s[3], s[4], s[5]);
}

function stringToDate(s) {
	s = s.split(/[-: ]/);
	return new Date(s[0], s[1] - 1, s[2], s[3], s[4], s[5]);
}

function build_article_cell(article) {
	var title = article.get('title');
	var page_url = article.get('page_url');
	var description = article.get('description');

	var max_str_length = 125;

	if (title.length > max_str_length / 2) {
		title = title.substring(0, max_str_length / 2);
		title += "...";
	}

	if (description.length > max_str_length) {
		description = description.substring(0, max_str_length);
		description += " ...";
	}


	var image = article.get('images')[0].url_size3;

	var article = $('<div>').prop({
		'class': 'article'
	});

	var article_image = $('<img>').prop({
		'class': 'article_image',
		'src': image
	});

	var article_title = $('<div>').prop({
		'class': 'article_title'
	});

	var h3 = $('<h3>');

	var title_link = $('<a>').prop({
		'href': page_url
	});

	var description_link = $('<a>').prop({
		'href': page_url,
		'class': 'more'
	});

	var short_description = $('<p>').html(description);

	var more = $('<h5>');
	
	more.prop({
		'class': 'more'
	});
	
	description_link.html('MORE');
	
	more.append(description_link)

	if (description.length > 0) {
		short_description.append(more);
	}

	title_link.html(title);

	h3.append(title_link);

	article_title.append(h3).append(short_description);

	return article.append(article_image).append(article_title);
}

$(document).ready(function() {
	var articleGallery = new ArticleGallery();
});
